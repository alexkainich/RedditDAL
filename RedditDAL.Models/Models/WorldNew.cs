namespace RedditDAL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using RedditDAL.Models.Base;

    public partial class WorldNew : EntityBase
    {
        [StringLength(1000)]
        public string Title { get; set; }

        [StringLength(1000)]
        public string Link { get; set; }

        [StringLength(50)]
        public string Score { get; set; }

        [StringLength(50)]
        public string Date { get; set; }

        [StringLength(100)]
        public string Flair { get; set; }
    }
}
