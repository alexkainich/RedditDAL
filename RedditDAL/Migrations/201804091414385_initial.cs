namespace RedditDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorldNews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 1000),
                        Link = c.String(maxLength: 1000),
                        Score = c.String(maxLength: 50),
                        Date = c.String(maxLength: 50),
                        Flair = c.String(maxLength: 100, fixedLength: true),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WorldNews");
        }
    }
}
