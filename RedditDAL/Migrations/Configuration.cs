namespace RedditDAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;

    using RedditDAL.Models;


    internal sealed class Configuration : DbMigrationsConfiguration<RedditDAL.EF.RedditEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RedditDAL.EF.RedditEntities context)
        {

        }
    }
}
