namespace RedditDAL.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using RedditDAL.Models;
    using System.Data.Entity.Infrastructure.Interception;
    using RedditDAL.Interception;
    using System.Data.Entity.Core.Objects;

    public partial class RedditEntities : DbContext
    {
        public RedditEntities()
            : base("name=RedditEntities")
        {
            //Interception!
            //DbInterception.Add(new ConsoleWriterInterceptor());

            //Logger Interceptor!
            //DatabaseLogger.StartLogging();
            //DbInterception.Add(DatabaseLogger);

            //Custom Interceptor!
            //var context = (this as IObjectContextAdapter).ObjectContext;
            //context.ObjectMaterialized += OnObjectMaterialized;
            //context.SavingChanges += OnSavingChanges;
        }

        //fires up immediately after a model's properties are populates by EF and just before the context servers it up to the calling code (p903)
        private void OnObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
        }

        //fires on the SavingChanges event just before the database is updated
        private void OnSavingChanges(object sender, EventArgs eventArgs)
        {
            //Sender is of type ObjectContext.  Can get current and original values, and 
            //   cancel /modify the save operation as desired.
            var context = sender as ObjectContext;
            if (context == null) return;
            foreach (ObjectStateEntry item in
                context.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Added))
            {
                //Do something important here
                if ((item.Entity as WorldNew) != null)
                {
                    var entity = (WorldNew)item.Entity;
                    //something
                }
            }

        }

        protected override void Dispose(bool disposing)
        {
            //DbInterception.Remove(DatabaseLogger);
            //DatabaseLogger.StopLogging();
            //base.Dispose(disposing);
        }

        public virtual DbSet<WorldNew> WorldNews { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorldNew>()
                .Property(e => e.Flair)
                .IsFixedLength();
        }
    }
}
